#ifndef CORE_DYNTYPE_HPP
#define CORE_DYNTYPE_HPP

void* dyntypeNew(char type);
void dyntypeDelete(void* val, char type);

#endif /* end of include guard: CORE_DYNTYPE_HPP */
